﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[ExecuteInEditMode]
public class DistanceAdjust : MonoBehaviour
{

    [Space]
    [Tooltip("X: length Y:width (meters)")]
    [SerializeField]
    Vector2 m_size;

    
    [SerializeField]
    float m_xAxis = 0f;
    [SerializeField]
    float m_zAxis = 0f;
    [SerializeField]
    Transform m_target;

    [MenuItem("Window/OnEnable")]
    void OnEnable()
    {
        if (m_size != Vector2.zero)
        {
            transform.localScale = new Vector3(m_size.x, transform.localScale.y, m_size.y);
        }
        if (m_target != null)
        {
            if (m_xAxis!=0f)
            {
                transform.position = new Vector3(m_target.position.x + m_xAxis,transform.position.y,transform.position.z);
            }
            else if (m_zAxis!=0f)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, m_target.position.z + m_zAxis);
            }
        }
    }

}
