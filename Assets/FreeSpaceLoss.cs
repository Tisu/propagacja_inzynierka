﻿using UnityEngine;
using System.Collections;

public class FreeSpaceLoss
{
    
    const float TRANSMITER_POWER = 20f; // in dBm
    public static readonly float[] MIN_RECEIVED_POWER = new float[] { -84f, -87f, -90f, -93f };
    eFrequency m_frequency;
    eDistanceUnit m_distance_unit;
    public FreeSpaceLoss(eFrequency a_frequency, eDistanceUnit a_distance_unit)
    {
#if DEBUG
        Debug.Log(a_distance_unit.ToString());
#endif
        m_frequency = a_frequency;
        m_distance_unit = a_distance_unit;
        m_speed_change_unity_distance = new float[MIN_RECEIVED_POWER.Length];
        for (int i = 0; i < m_speed_change_unity_distance.Length; i++)
        {
            if (a_distance_unit == eDistanceUnit.kilometers)
            {
                m_speed_change_unity_distance[i] = DistanceStruct.KiloMeterToUnityDistance(DistanceSpeedChange(MIN_RECEIVED_POWER[i]));
            }
            else if (a_distance_unit == eDistanceUnit.meters)
            {
                m_speed_change_unity_distance[i] = DistanceStruct.MeterToUnityDistance(DistanceSpeedChange(MIN_RECEIVED_POWER[i]));
            }
            else
            {
                Debug.Assert(false, "other unit not implemented yet");
                Debug.Break();
            }
        }
#if DEBUG
        PrintDistances();
#endif
    }
    public enum eFrequency
    {
        b_n_g = 2440,
        a_n_ac = 5300
    }
    public enum eDistanceUnit
    {
        kilometers = 1,
        meters = 1000
    }

    public float[] m_speed_change_unity_distance;
    /// <summary>
    /// to calculate max loss value in order to given power
    /// </summary>
    /// <param name="a_power_needed"></param>
    /// <returns></returns>
    float LossInPowerFunction(float a_min_power_received)
    {
        return TRANSMITER_POWER - a_min_power_received;
    }
    /// <summary>
    /// Distance which is important due to speed change in this point
    /// </summary>
    /// <param name="a_frequency"></param>
    /// <returns></returns>
    float DistanceSpeedChange(float a_min_power_received)
    {
        switch (m_distance_unit)
        {
            case eDistanceUnit.kilometers:
                {

                    return Mathf.Pow(10f, Exp(a_min_power_received));

                }
            case eDistanceUnit.meters:
                {

                    return Mathf.Pow(10f, Exp(a_min_power_received)) * 1000;
                }
            default:
                {
                    Debug.Assert(false, "wrong eDistanceUnit");
                    return 0;
                }
        }
    }
    public float dBToUnityDistanceLoss(float a_min_power_received, float a_dB)
    {
        switch (m_distance_unit)
        {
            case eDistanceUnit.kilometers:
                {
                    return DistanceStruct.KiloMeterToUnityDistance(Mathf.Pow(10f, Exp(a_min_power_received, a_dB)));
                }
            case eDistanceUnit.meters:
                {
                    return DistanceStruct.MeterToUnityDistance(Mathf.Pow(10f, Exp(a_min_power_received, a_dB)) * 1000);
                }
            default:
                {
                    Debug.Assert(false, "wrong eDistanceUnit");
                    return 0;
                }
        }
    }
    float Exp(float a_min_power_received)
    {
        if (Draw.m_noise_figure)
        {            
            return (LossInPowerFunction(a_min_power_received) - 20 * Mathf.Log10((float)m_frequency) - 32.44f - 3f) / 20f;
        }
        else
        {
            return (LossInPowerFunction(a_min_power_received) - 20 * Mathf.Log10((float)m_frequency) - 32.44f) / 20f;
        }
    }
    float Exp(float a_min_power_received, float a_dB)
    {
        if (Draw.m_noise_figure)
        {
            return (LossInPowerFunction(a_min_power_received) - 20 * Mathf.Log10((float)m_frequency) - 32.44f - a_dB- 3f) / 20f;
        }
        else
        {
            return (LossInPowerFunction(a_min_power_received) - 20 * Mathf.Log10((float)m_frequency) - 32.44f - a_dB) / 20f;
        }
    }
#if DEBUG
    void PrintDistances()
    {
        foreach (var distance in m_speed_change_unity_distance)
        {
            Debug.Log(distance.ToString());
        }
    }
#endif
}
