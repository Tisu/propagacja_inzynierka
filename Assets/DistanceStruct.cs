﻿using UnityEngine;
using System.Collections;

public static class DistanceStruct
{
    const float UNIT_TO_DISTANCE_KM = 1f; // 1km = 1dist in Unity
    const float UNIT_TO_DISTANCE_M = 1f; //1m = 1dist in unity
    public const int PIXELS_TO_ONE_UNITY_UNIT = 100;

    public static float KiloMeterToUnityDistance(float a_kilometer)
    {
        return a_kilometer / UNIT_TO_DISTANCE_KM;
    }
   public static float MeterToUnityDistance(float a_meter)
    {
        return a_meter / UNIT_TO_DISTANCE_M;
    }
}
