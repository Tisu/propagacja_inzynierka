﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Draw : MonoBehaviour
{
    readonly Color[] COLOR_SPEED = new Color[] { Color.red, Color.green, Color.blue, Color.black };
    int SPEED_POSSIBILITY_COUNT;
#if DEBUG
    bool m_debug = true;
#endif
    struct PointToDraw
    {
        public PointToDraw(float a_distance, int a_color_index)
        {
            m_distance = a_distance;
            m_color_index = a_color_index;
        }

        public float m_distance;
        public int m_color_index;
    }
    [SerializeField]
    private uint m_line_count;

    public static bool m_noise_figure = true; // -6 dB fider loss

    List<PointToDraw> m_length_to_draw = new List<PointToDraw>();

    static Material m_line_material;

    int m_index_point_wall_hit = 0;
    bool m_last_point_without_obstacle = false;
    float m_obstacles_loss = 0;
    Vector3 m_last_hit_obstacle = Vector3.zero;
    FreeSpaceLoss m_free_space_loss = new FreeSpaceLoss(FreeSpaceLoss.eFrequency.b_n_g, FreeSpaceLoss.eDistanceUnit.meters);

    /// <summary>
    /// Funkcion that calculate propagation loss with obstacles
    /// </summary>
    /// <param name="a_min_power_received">min power received to gain appropriate transfer speed</param>
    /// <param name="a_dB">Sum loss on obstacle</param>
    /// <returns></returns>
    private delegate float LossCalculationWall(float a_min_power_received, float a_dB);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    /// <summary>
    /// Funkcion that calculate propagation loss without any obstacles
    /// </summary>
    /// <returns>Has to return distance in unity units</returns>
    private delegate float LossCalculation(float a_min_power_received);
    // Use this for initialization
    LossCalculation m_loss_calculation;*/
    // for now its only possible to use free space calculation
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    LossCalculationWall m_loss_calculation_with_obstacle;

    void Awake()
    {
        SPEED_POSSIBILITY_COUNT = COLOR_SPEED.Length;

        if (COLOR_SPEED.Length != m_free_space_loss.m_speed_change_unity_distance.Length)
        {
            System.Diagnostics.Debug.Assert(false, "Too few color argument");
            Debug.Break();
        }
        m_loss_calculation_with_obstacle += m_free_space_loss.dBToUnityDistanceLoss;
    }
    static void CreateLineMaterial()
    {
        if (!m_line_material)
        {
            var shader = Shader.Find("Hidden/Internal-Colored");
            m_line_material = new Material(shader);
            m_line_material.hideFlags = HideFlags.HideAndDontSave;

            m_line_material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            m_line_material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            m_line_material.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            m_line_material.SetInt("_ZWrote", 0);
        }
    }
    void OnRenderObject()
    {
        CreateLineMaterial();
        m_line_material.SetPass(0);
        GL.PushMatrix();
        GL.MultMatrix(transform.localToWorldMatrix);
        GL.Begin(GL.LINES);
        Vector3 start_point;
        Vector3 end_point;
        RaycastHit[] hit;
        for (int i = 0; i < m_line_count; i++)
        {

            float circle_slice = i / (float)m_line_count;
            float angle = circle_slice * Mathf.PI * 2;
            start_point = end_point = new Vector3(0, 0, 0);
            // Vector3 direction = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));
            m_length_to_draw.Clear();
            m_last_hit_obstacle = transform.position;
            if (CheckAngle(angle, out hit, m_free_space_loss.m_speed_change_unity_distance[m_free_space_loss.m_speed_change_unity_distance.Length - 1])) //checking if obstacle on the way by given distance
            {
                m_last_hit_obstacle = hit[0].transform.position;
                for (int speed_change_points = 0; speed_change_points < m_free_space_loss.m_speed_change_unity_distance.Length; speed_change_points++)
                {
                    if (m_free_space_loss.m_speed_change_unity_distance[speed_change_points] > hit[0].distance)
                    {
                        m_obstacles_loss = 0;
                        int start_drawing_index = 0;
                        CalculatePointsToDraw(ref hit, speed_change_points, angle);
                        if (speed_change_points == 0)
                        {
                            start_point = end_point = Vector3.zero;
                        }
                        else
                        {
                            start_point = end_point = new Vector3(Mathf.Cos(angle) * m_length_to_draw[0].m_distance, 0, Mathf.Sin(angle) * m_length_to_draw[0].m_distance);
                            start_drawing_index = 1;
                        }
                        for (int j = start_drawing_index; j < m_length_to_draw.Count; j++)
                        {
                            start_point = end_point;
                            end_point = new Vector3(Mathf.Cos(angle) * m_length_to_draw[j].m_distance, 0, Mathf.Sin(angle) * m_length_to_draw[j].m_distance);
                            DrawLine(COLOR_SPEED[m_length_to_draw[j].m_color_index], start_point, end_point);
                        }
                        break; // no need to iterate throu speed change points because its calculated in CalculatePointsToDraw
                    }
                    else // when no wall is hit with specific speed
                    {
                        m_length_to_draw.Clear();
                        start_point = end_point;
                        end_point = new Vector3(Mathf.Cos(angle) * m_free_space_loss.m_speed_change_unity_distance[speed_change_points], 0, Mathf.Sin(angle) * m_free_space_loss.m_speed_change_unity_distance[speed_change_points]);
                        m_last_point_without_obstacle = true;
                        m_length_to_draw.Add(new PointToDraw(end_point.magnitude, speed_change_points + 1));
                        DrawLine(COLOR_SPEED[speed_change_points], start_point, end_point);
                    }
                }

            }
            else
            {
                for (int speed_change_points = 0; speed_change_points < m_free_space_loss.m_speed_change_unity_distance.Length; speed_change_points++)
                {
                    start_point = end_point;
                    end_point = new Vector3(Mathf.Cos(angle) * m_free_space_loss.m_speed_change_unity_distance[speed_change_points], 0, Mathf.Sin(angle) * m_free_space_loss.m_speed_change_unity_distance[speed_change_points]);
                    DrawLine(COLOR_SPEED[speed_change_points], start_point, end_point);
                }
            }
        }
        GL.End();
        GL.PopMatrix();
    }

    private void CalculatePointsToDraw(ref RaycastHit[] a_hit, int a_speed_change_point, float a_angle)
    {
        if (a_speed_change_point > SPEED_POSSIBILITY_COUNT - 1)
        {
            return;
        }
        CheckRaycast(ref a_hit);
        if (m_length_to_draw.Count == 0 || m_last_point_without_obstacle)
        {
            m_length_to_draw.Add(new PointToDraw(a_hit[0].distance, a_speed_change_point));
            m_last_point_without_obstacle = false;
        }
        else
        {
            m_length_to_draw.Add(new PointToDraw(a_hit[0].distance + m_length_to_draw[m_index_point_wall_hit].m_distance, a_speed_change_point));
        }
        bool outside_wall;
        m_obstacles_loss += ObstacleInformation(ref a_hit[0], out outside_wall);
        if (outside_wall)
        {
//#if DEBUG
//            if (a_hit.Length > 1 && m_debug)
//            {
//                Debug.Assert(false, "Are you sure this is the last wall? object: " + a_hit[a_hit.Length - 1].transform.name); // if more walls on the way
//                m_debug = false;
//            }
//#endif
            return; // if this is outside wall dont do unnessecary calculation, stop drawing
        }

        m_last_hit_obstacle = a_hit[0].transform.position;
        m_index_point_wall_hit = m_length_to_draw.Count - 1;

        float ray_length = 0;
        for (int left_point_to_calculate = a_speed_change_point; left_point_to_calculate < SPEED_POSSIBILITY_COUNT; left_point_to_calculate++)
        {
            ray_length = m_loss_calculation_with_obstacle(FreeSpaceLoss.MIN_RECEIVED_POWER[left_point_to_calculate], m_obstacles_loss);
            if (m_length_to_draw.Last().m_distance < ray_length)
            {
                m_length_to_draw.Add(new PointToDraw(ray_length, left_point_to_calculate));
            }
        }

        RaycastHit[] new_hit;
        if (CheckAngle(a_angle, out new_hit, m_length_to_draw.Last().m_distance - m_length_to_draw[m_index_point_wall_hit].m_distance))
        {
            int color_index = m_length_to_draw.Where(x => x.m_distance > (new_hit[0].distance + m_length_to_draw[m_index_point_wall_hit].m_distance)).First().m_color_index;
            m_length_to_draw.RemoveAll(x => x.m_distance > (new_hit[0].distance + m_length_to_draw[m_index_point_wall_hit].m_distance));

            CalculatePointsToDraw(ref new_hit, color_index, a_angle);
        }
        else // no more  wall on the way
        {
            return;
        }
    }

    private void CheckRaycast(ref RaycastHit[] a_hit)
    {
        if (a_hit.Length > 0)
        {
            a_hit[0] = a_hit.OrderBy(x => x.distance).First();
        }
        else
        {
            Debug.Log("raycast empty");
            Debug.Break();
        }
    }

    private float ObstacleInformation(ref RaycastHit a_hit, out bool a_outside_wall)
    {
        var wall_obstacle_script = a_hit.transform.GetComponent<WallObstacle>();
        if (wall_obstacle_script)
        {
            a_outside_wall = wall_obstacle_script.m_outside_wall;
            return wall_obstacle_script.m_loss;
        }
        else
        {
            Debug.Log("WallObstacle script not attached on: " + a_hit.transform.gameObject.name);
            a_outside_wall = false;
            Debug.Break();
            return 0f;
        }
    }

    /// <summary>
    /// use to check obstacles
    /// </summary>
    /// <param name="a_angle"></param>
    /// <param name="a_vector_magnitude"></param>
    /// <param name="a_hit"></param>
    /// <param name="a_distance"></param>
    /// <returns></returns>
    private bool CheckAngle(float a_angle, out RaycastHit[] a_hit, float a_distance)
    {
        a_hit = Physics.RaycastAll(m_last_hit_obstacle, new Vector3(Mathf.Cos(a_angle), 0, Mathf.Sin(a_angle)), a_distance);
        for (int i = 0; i < a_hit.Length; i++)
        {
            if (a_hit[i].transform.tag.Equals("Obstacle"))
            {
                a_hit[0] = a_hit[i];
                return true;
            }
        }
        return false;
    }
    void DrawLine(Color a_color, Vector3 a_start_point, Vector3 a_end_point)
    {
        GL.Color(a_color);
        GL.Vertex3(a_start_point.x, a_start_point.y, a_start_point.z);
        GL.Vertex3(a_end_point.x, a_end_point.y, a_end_point.z);
    }
}
